<?php
session_start();
// endを押したときの処理
$_SESSION = array();
if (isset($_COOKIE[session_name()])) {
    setcookie(session_name(), '', time()-42000, '/');
}
session_destroy();
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name=viewport content="width=device-width, initial-scale=1">
    <title>like line</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <div class="chat_title">like line</div>
    </header>
    <article>
        <div class="logout_text">
        Thank you for using!
        </div>
        <a href="index.php" class="restart">
        Once again
        </a>
    </article>
</body>
</html>