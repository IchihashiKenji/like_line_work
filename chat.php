<?php
session_start();

require_once __DIR__ . '/class/Validation.php';
$getValidation = new Validation();

// データベースへ接続
function databaseOperation($messageInt, $created) {
    try {
        $dbh = new PDO('mysql:host=localhost;charset=utf8;dbname=like_line','dbuser','vagrant');
    } catch (PDOException $e) {
        var_dump($e->getMessage());
        exit;
    }

    // データ挿入
    $stmt = $dbh->prepare("insert into line_message (session_id,user_name,message,created) values (:session_id,:user_name,:message,:created)");
    $stmt->execute(array(":session_id"=>$_SESSION['sessionId'],":user_name"=>$_SESSION['userName'],":message"=>$_POST['message'],":created"=>$created));

    // 切断
    $dbh = null;
}
// セッションチェック
if (session_id() != $_SESSION['sessionId']) {
    header("Location: ./index.php?error");
}
// メッセージ投稿ボタンを押した後の処理
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['message'])) {

    // トークンのチェック
    $getValidation->checkToken();

    $message = $_POST['message'];
    $messageInt = mb_strlen($message);

    // バリデーションの値を設定
    $data = array(
        'userName' => $_SESSION['userName'],
        'message'  => $message
    );
    $getValidation->set($data);

    if ($getValidation->noMessage() == false) {
        $errorMessage = $getValidation->validation;
    } elseif ($getValidation->overMessage() == false) {
        $errorMessage = $getValidation->validation;
    }
    // 投稿時間の取得
    $created = date("Y-m-d H:i:s", time());

    // データベースへ接続
    if (empty($getValidation->validation) == true) {
        databaseOperation($messageInt,$created);
    }
}
// データベースのメッセージの読み込み（表示のため）
try {
    $dbh = new PDO('mysql:host=localhost;charset=utf8;dbname=like_line','dbuser','vagrant');
} catch (PDOException $e) {
    var_dump($e->getMessage());
    exit;
}
$sth = $dbh->prepare("select * from line_message");
$sth->execute();

// 結果セットに残っている全ての行をフェッチする
$result = $sth->fetchAll();
$dbh = null;
// 誰のメッセージか分ける
function whoseMessage($messageData) {
    if ($messageData['session_id'] == $_SESSION['sessionId']) {
        $whoseMessage = "mine";
    } else {
        $whoseMessage = "others";
    }
    return $whoseMessage;
}

$getValidation->setToken();

?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name=viewport content="width=device-width, initial-scale=1">
    <title>like line</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <form action="logout.php" method="post" class="end">
            <input type="submit" value="end" class="end_button">
        </form>
        <div class="chat_title">like line</div>
        <form action="chat.php" method="post" class="reload">
            <input type="submit" value="reload" class="reload_button">
        </form>
    </header>
    <ul>
<?php
// 配列０からなくなるまで繰り返し
foreach ($result as $messageData) {
    $messageClass = whoseMessage($messageData);
    $date = $messageData['created'];
?>
        <li class="balloon_space <?php echo $messageClass; ?>">
            <div class="user_name">
<?php
    // メッセージ上の名前を出すか否か
    if ($messageClass == "others") {
        echo htmlspecialchars($messageData['user_name']);
    }
?>
            </div>
            <div class="box <?php echo $messageClass; ?>">
                <div class="message <?php echo $messageClass; ?>">
<?php
echo "&ensp;" . htmlspecialchars($messageData['message']);
?>
                </div>
                <div class="datetime <?php echo $messageClass; ?>">
<?php
// 投稿日時の出力
echo date('m'.'/'.'d', strtotime($date))
?>
                <br>
<?php
echo date('G'.':'.'i', strtotime($date));
?>
                </div>
            </div>
        </li>
<?php
}
?>
    </ul>
    <footer id="footer">
        <div class="error_message_chat_page">

<?php
if (empty($errorMessage) == false) {
    echo $errorMessage;
}
?>
        </div>
        <div class="user_name">
<?php
echo htmlspecialchars($_SESSION['userName']);
?>
        </div>
        <form action="chat.php" method="post" class="message_form" autocomplete="off">
            <input type="text" name="message" class="message_write">
            <input type="submit" value="▷" class="message_posting">
            <input type="hidden" name="token" value="<?php echo $getValidation->h($_SESSION['token']); ?>">
        </form>
    </footer>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script>
        $(function() {
            window.scrollTo(0,document.body.scrollHeight);
        });
    </script>
    <script type="text/javascript">
        var userAgent = window.navigator.userAgent.toLowerCase();
        if (userAgent.indexOf('chrome') != -1 && userAgent.indexOf('version') != -1) {
            $(function(){
                $('.message_write').on('focus', function(){
                    var box = document.getElementById('footer');
                    box.className = "footer_expand";
                });
            });
            $(function(){
                $('.message_write').on('blur', function(){
                    document.getElementById('footer').classList.remove('footer_expand');
                    console.log(document.getElementById('footer').classList.contains('footer_expand'));
                });
            });
        }
    </script>
</body>
</html>